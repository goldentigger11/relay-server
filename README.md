SDC 

# Introduction 

This project will allow the user to drive the autonomous golf cart using the VR headset and controllers. Haptic gloves are also worn to give a realsitic feeling of the vibrations and bumps that are experienced by the car. 


# Set up 
### create a firebase DB and put the lock key hena 
## On SDC Computer side
1. > python double_camera_interface.py

        Make sure the 3 cameras on the car are adjusted in this sequence: 
        The drop camera, main camera, back camera from left to right. 

        Make sure all 3 cameras are working and detected by openCV to avoid errors. 

![Camera Setup](camera_setup.png)

2. > python "gloves reciver and controller.py"

        Before running this file. Burn the arduino code "SDC" on the arduino. Make sure the Port and the baudrate is the same as the one the arduino code is running on. 

![Refresh rate](Adjust_refreshrate_of_gloves.jpg)


3. > python "reciver and car controller.py"


 

    
4. > python "sensors sender arduino.py"

        Before running this file. Burn the arduino code "SDC" on the arduino. Make sure the Port and the baudrate is the same as the one the arduino code is running on. 


![Serial](serial.png)


## On VR lab PC

1. open **steamVR**, 
2. Turn on headset and the VR controllers, SteamVR Bases.  
3. Open unity hub and choose **SLC** project 
4. run the project

## Open a Stream from both computers 

STREAM:
https://vdo.ninja/alpha/?push=SDC&screensharefps=60&chunked=10&screensharecontenthint=motion&height=360

VIEW:
https://vdo.ninja/alpha/?view=SDC&codec=vp8&buffer=0&keyframerate=10

Use the above links to open an "optimized" stream. 

Parameters to adjust:

        Adjust these in the link itself. 

0. **view**/**push**: The ID of the stream. 
1. **Chunked**: bit rate.
2. **Height**: height of stream
3. **&turn=false**: Force disable for the relay server and start peer to peer connection if possible. NB: This only works after port forwarding has been set. 

The Streamer PC open this from **Chrome**.

The Viewr opens the **ElectronCapture** app. 

"https://github.com/steveseguin/electroncapture"

To monitor the delay. go to streamer pc and pres CTRL+click the stream. make sure the Rtt time is less than 300ms. 


## GLOVE code in the haptic_glove21

Wire the glove as shown in the circuit diagram.

Burn the code on arduino. 

Run the python file named: "gloves reciver and controller.py" 

Put on the glove and make sure the car is sending the Vibrations data.


