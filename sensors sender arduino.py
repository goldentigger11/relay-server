from tokenize import Number
import serial                                                              #Serial imported for Serial communication
import firebase_admin
from firebase_admin import credentials, db
from firebase_rtdb_pagination import FirebaseRTDBPagination
import json
import time   
import numpy as np
import struct

cert = '/home/pc-car/Downloads/drive-71fa6-firebase-adminsdk-lsczy-62d18f65e0.json'
#cred = credentials.Certificate("drive-71fa6-firebase-adminsdk-lsczy-5cca64c6f9.json")

cred = credentials.Certificate(cert)
app = firebase_admin.initialize_app(cred, {'databaseURL': 'https://drive-71fa6-default-rtdb.firebaseio.com/'})

ref = db.reference("/sensors")
                                                 #Required to use delay functions   
ArduinoUnoSerial = serial.Serial('/dev/ttyACM1',115200)   
ArduinoUnoSerial.flushInput()
while True:    #Create Serial port object called ArduinoUnoSerialData time.sleep(2)                                                             #wait for 2 secounds for the communication to get established
    try:
        string = ArduinoUnoSerial.readline().decode("utf-8") .rstrip()
        ArduinoUnoSerial.flushInput()

        left_lidar, right_lidar, vibrations = string.split(',')[0:3]
        left_lidar, right_lidar, vibrations = float(left_lidar), float(right_lidar), float(vibrations)
    
        # add values to firebse
        values = {
            'vibrations': vibrations,
            'left lidar': left_lidar,
            'right lidar': right_lidar,
        }
        ref.set(values)
    except Exception as e:
        print(f'err{e}')