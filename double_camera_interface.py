# import the opencv library
import cv2
from openCV_Human_detection.pedestrian_detection_project_code.mini_yolo import Yolo_mini
import numpy as np




# define a video capture object
vid = cv2.VideoCapture(2)
vid.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
vid.set(cv2.CAP_PROP_FRAME_HEIGHT, 240) 



vid2 = cv2.VideoCapture(4)
vid2.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
vid2.set(cv2.CAP_PROP_FRAME_HEIGHT, 240) 

vid3 = cv2.VideoCapture(2)
vid3.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
vid3.set(cv2.CAP_PROP_FRAME_HEIGHT, 240) 

yolo = Yolo_mini()
yolo.try_on_images_folder()
while(True):
	
    # Capture the video frame
    # by frame
    ret, frame1 = vid.read()
    ret, frame2 = vid2.read()
    ret, frame3 = vid.read()
    frame2 = yolo.draw_bounding_boxes(frame2)
    
    # frame = np.concatenate((frame1,frame2,frame3), axis=1)

    # frame = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)
    frame = frame2
    # Display the resulting frame
    cv2.imshow('frame', frame)

    # the 'q' button is set as the
    # quitting button you may use any
    # desired button of your choice
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# After the loop release the cap object
vid.release()
# Destroy all the windows
cv2.destroyAllWindows()
