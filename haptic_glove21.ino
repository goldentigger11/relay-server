

int RMotor_1 = 2;
int RMotor_2 = 3;
int LMotor_1 = 4;
int LMotor_2 = 5;
int REnable = 10;
int LEnable = 11;


int RMotor_11 = 7;
int RMotor_22 = 8;
int LMotor_11 = 12;
int LMotor_22 = 13;
int REnable2 = 6;
int LEnable2 = 9;

int motor_speed;



void setup() {
  Serial.begin(115200);
  Serial.setTimeout(200);
  Serial.println("GPIO test!");
  pinMode(RMotor_1, OUTPUT);
  pinMode(RMotor_2, OUTPUT);
  pinMode(LMotor_1, OUTPUT);
  pinMode(LMotor_2, OUTPUT);
  pinMode(REnable, OUTPUT);
  pinMode(LEnable, OUTPUT);

  pinMode(RMotor_11, OUTPUT);
  pinMode(RMotor_22, OUTPUT);
  pinMode(LMotor_11, OUTPUT);
  pinMode(LMotor_22, OUTPUT);
  pinMode(REnable2, OUTPUT);
  pinMode(LEnable2, OUTPUT);
  
  pinMode(A0,INPUT);
  
  analogWrite(10, 210);
  analogWrite(11, 210);
  analogWrite(6, 210);
  analogWrite(9, 210);

}

void loop() {

 int x = Serial.readString().toInt();
 x=x-7300;
 x= abs(x);
 x= constrain(x,400,1700);
 //int pot= analogRead(A0);
 int poto= map(x,0,1700,0,255);
// Serial.println(pot);

 if(x<850){
  analogWrite(10, poto);
  analogWrite(11, 0);
  set1();
  set11();
  }
 else{
  analogWrite(10, poto);
  analogWrite(11, poto);
  set2();
  set22();
 }
 }

 void set1() {
  digitalWrite(RMotor_1, LOW);
  digitalWrite(RMotor_2, HIGH);
  digitalWrite(LMotor_1, LOW);
  digitalWrite(LMotor_2, HIGH);
}

void set2() {
  digitalWrite(RMotor_1, LOW);
  digitalWrite(RMotor_2, HIGH);
  digitalWrite(LMotor_1, HIGH);
  digitalWrite(LMotor_2, LOW);
}


 void set11() {
  digitalWrite(RMotor_11, LOW);
  digitalWrite(RMotor_22, HIGH);
  digitalWrite(LMotor_11, LOW);
  digitalWrite(LMotor_22, HIGH);
}

void set22() {
  digitalWrite(RMotor_11, LOW);
  digitalWrite(RMotor_22, HIGH);
  digitalWrite(LMotor_11, HIGH);
  digitalWrite(LMotor_22, LOW);
}
