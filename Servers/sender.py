import json
import socket

UDP_IP = "95.179.139.38"
# UDP_IP = socket.gethostbyname(socket.gethostname())


UDP_PORT = 1234
MESSAGE = json.dumps({
    "name":"karim",
    "age": 12,
})


print("UDP target IP:", UDP_IP)
print("UDP target port:", UDP_PORT)
print("message:", MESSAGE)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
sock.sendto(bytes(MESSAGE, "utf-8"), (UDP_IP, UDP_PORT))
