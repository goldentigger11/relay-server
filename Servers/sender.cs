static void SendUdp(int srcPort, string dstIp, int dstPort, byte[] data)
{
    using (UdpClient c = new UdpClient(srcPort))
        c.Send(data, data.Length, dstIp, dstPort);
}

SendUdp(11000, "192.168.2.255", 11000, Encoding.ASCII.GetBytes("Hello!"));
print("Done sending");
