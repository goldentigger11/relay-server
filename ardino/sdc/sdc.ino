#include <TFMPlus.h>  // Include TFMini Plus Library v1.5.0
TFMPlus tfmP1;         // Create a TFMini Plus object
TFMPlus tfmP2;         // Create a TFMini Plus object
TFMPlus tfmP3;         // Create a TFMini Plus object

#include <basicMPU6050.h> 

// Create instance
basicMPU6050<> imu;
#include "printf.h"   // Modified to support Intel based Arduino
                      // devices such as the Galileo. Download from:
                      // https://github.com/spaniakos/AES/blob/master/printf.h

// The Software Serial library is an alternative for devices that
// have only one hardware serial port. Delete the comment slashes
// on lines 37 and 38 to invoke the library, and be sure to choose
// the correct RX and TX pins: pins 10 and 11 in this example. Then
// in the 'setup' section, change the name of the hardware 'Serial2'
// port to match the name of your software serial port, such as:
// 'mySerial.begin(115200); etc.

#include <SoftwareSerial.h>       
// Accelerometer init
#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>

Adafruit_MPU6050 mpu;



SoftwareSerial mySerial( 14, 15);   
                                    
void setup()
{
  // LIDAR SETUP ----------------------------------------------------------------------------------------
     imu.setup();
    Serial.begin( 115200);   // Intialize terminal serial port
    delay(20);               // Give port time to initalize
    printf_begin();          // Initialize printf.
    printf("\r\nTFMPlus Library Example - 10SEP2021\r\n");  // say 'hello'

    Serial1.begin( 115200);  // Initialize TFMPLus device serial port.
    delay(20);               // Give port time to initalize
    tfmP1.begin( &Serial1); // Initialize device library object and...
                             // pass device serial port to the object.

    Serial2.begin( 115200);  // Initialize TFMPLus device serial port.
    delay(20);               // Give port time to initalize
    tfmP2.begin( &Serial2);

    Serial3.begin( 115200);  // Initialize TFMPLus device serial port.
    delay(20);               // Give port time to initalize
    tfmP3.begin( &Serial3);

    // Send some example commands to the TFMini-Plus
    // - - Perform a system reset - - - - - - - - - - -
    printf( "Soft reset: ");
    if( tfmP1.sendCommand( SOFT_RESET, 0))
    {
     //   printf( "passed.\r\n");
    }
    else tfmP1.printReply();
  
    delay(500);  // added to allow the System Rest enough time to complete

  // - - Display the firmware version - - - - - - - - -
    printf( "Firmware version: ");
    if( tfmP1.sendCommand( GET_FIRMWARE_VERSION, 0))
    {
//        printf( "%1u.", tfmP1.version[ 0]); // print three single numbers
//        printf( "%1u.", tfmP1.version[ 1]); // each separated by a dot
//        printf( "%1u\r\n", tfmP1.version[ 2]);
    }
    else tfmP1.printReply();
    // - - Set the data frame-rate to 20Hz - - - - - - - -
    printf( "Data-Frame rate: ");
    if( tfmP1.sendCommand( SET_FRAME_RATE, FRAME_20))
    {
    //    printf( "%2uHz.\r\n", FRAME_20);
    }
    else tfmP1.printReply();
    // - - - - - - - - - - - - - - - - - - - - - - - -

/*  // - - - - - - - - - - - - - - - - - - - - - - - -  
    // The next two commands may be used to switch the device 
    // into I2C mode.  This sketch will no longer receive UART
    // (serial) data.  The 'TFMPI2C_example' sketch in the 
    // TFMPI2C Library can be used to switch the device back
    // to UART mode.
    // Don't forget to switch the cables, too.
    // - - - - - - - - - - - - - - - - - - - - - - - -
    printf( "Set I2C Mode: ");
    if( tfmP.sendCommand( SET_I2C_MODE, 0))
    {
        printf( "mode set.\r\n");
    }
    else tfmP.printReply();
    printf( "Save settings: ");
    if( tfmP.sendCommand( SAVE_SETTINGS, 0))
    {
        printf( "saved.\r\n");
    }
    else tfmP.printReply();
    // - - - - - - - - - - - - - - - - - - - - - - - -    
*/

  //  ACCELEROMETER SET UP ------------------------------------------------------------------------------------
    // Try to initialize!
  if (!mpu.begin()) {
    Serial.println("Failed to find MPU6050 chip");
    while (1) {
      delay(10);
    }
  }
  Serial.println("MPU6050 Found!");

  mpu.setAccelerometerRange(MPU6050_RANGE_8_G);
  Serial.print("Accelerometer range set to: ");
  switch (mpu.getAccelerometerRange()) {
  case MPU6050_RANGE_2_G:
    Serial.println("+-2G");
    break;
  case MPU6050_RANGE_4_G:
    Serial.println("+-4G");
    break;
  case MPU6050_RANGE_8_G:
    Serial.println("+-8G");
    break;
  case MPU6050_RANGE_16_G:
    Serial.println("+-16G");
    break;
  }
  mpu.setGyroRange(MPU6050_RANGE_500_DEG);
  Serial.print("Gyro range set to: ");
  switch (mpu.getGyroRange()) {
  case MPU6050_RANGE_250_DEG:
    Serial.println("+- 250 deg/s");
    break;
  case MPU6050_RANGE_500_DEG:
    Serial.println("+- 500 deg/s");
    break;
  case MPU6050_RANGE_1000_DEG:
    Serial.println("+- 1000 deg/s");
    break;
  case MPU6050_RANGE_2000_DEG:
    Serial.println("+- 2000 deg/s");
    break;
  }

  mpu.setFilterBandwidth(MPU6050_BAND_21_HZ);
  Serial.print("Filter bandwidth set to: ");
  switch (mpu.getFilterBandwidth()) {
  case MPU6050_BAND_260_HZ:
    Serial.println("260 Hz");
    break;
  case MPU6050_BAND_184_HZ:
    Serial.println("184 Hz");
    break;
  case MPU6050_BAND_94_HZ:
    Serial.println("94 Hz");
    break;
  case MPU6050_BAND_44_HZ:
    Serial.println("44 Hz");
    break;
  case MPU6050_BAND_21_HZ:
    Serial.println("21 Hz");
    break;
  case MPU6050_BAND_10_HZ:
    Serial.println("10 Hz");
    break;
  case MPU6050_BAND_5_HZ:
    Serial.println("5 Hz");
    break;
  }


  delay(500);            // And wait for half a second.
}

// Initialize variables
int16_t tfDist = 0;    // Distance to object in centimeters
int16_t tfFlux = 0;    // Strength or quality of return signal
int16_t tfTemp = 0;    // Internal temperature of Lidar sensor chip

int16_t tfDist2 = 0;    // Distance to object in centimeters
int16_t tfFlux2 = 0;    // Strength or quality of return signal
int16_t tfTemp2 = 0;    // Internal temperature of Lidar sensor chip

int16_t tfDist3 = 0;    // Distance to object in centimeters
int16_t tfFlux3 = 0;    // Strength or quality of return signal
int16_t tfTemp3 = 0;    // Internal temperature of Lidar sensor chip

// Use the 'getData' function to pass back device data.
void loop()
{

    if( tfmP1.getData( tfDist, tfFlux, tfTemp)) // Get data from the device.
    {
     // printf( "Dist1:%04icm ", tfDist);   // display distance,
     // printf( "Flux:%05i ",   tfFlux);   // display signal strength/quality,
     // printf( "Temp:%2i%s",  tfTemp, "C");   // display temperature,
//      printf( "\r\n");                   // end-of-line.
    }
    else                  // If the command fails...
    {
      tfmP1.printFrame();  // display the error and HEX dataa
    }

     
     
     if( tfmP2.getData( tfDist2, tfFlux2, tfTemp2)) // Get data from the device.
    {
      //printf( "Dist2:%04icm ", tfDist2);   // display distance,
     // printf( "Flux:%05i ",   tfFlux2);   // display signal strength/quality,
     // printf( "Temp:%2i%s",  tfTemp2, "C");   // display temperature,
//      printf( "\r\n");                   // end-of-line.
    }
    else                  // If the command fails...
    {
      tfmP2.printFrame();  // display the error and HEX dataa
    }

     
     
     if( tfmP3.getData( tfDist3, tfFlux3, tfTemp3)) // Get data from the device.
    {
    //printf( "Dist3:%04icm ", tfDist3);   // display distance,
//      Serial.println("hello python");
     // printf( "Flux:%05i ",   tfFlux3);   // display signal strength/quality,
      //printf( "Temp:%2i%s",  tfTemp3, "C");   // display temperature,
//      printf( "\r\n");                   // end-of-line.
    }
    else                  // If the command fails...
    {
      tfmP3.printFrame();  // display the error and HEX dataa
    }


    // ACCELEROMETER READINGS --------------------------------------------------------

  /* Get new sensor events with the readings */
  sensors_event_t a, g, temp;
  mpu.getEvent(&a, &g, &temp);

  /* Print out the values */
//  Serial.print("Acceleration X: ");
//  Serial.print(a.acceleration.x);
//  Serial.print(", Y: ");
//  Serial.print(a.acceleration.y);
//  Serial.print(", Z: ");
//  Serial.print(a.acceleration.z);
//  Serial.println(" m/s^2");
//
//  Serial.print("Rotation X: ");
//  Serial.print(g.gyro.x);
//  Serial.print(", Y: ");
//  Serial.print(g.gyro.y);
//  Serial.print(", Z: ");
//  Serial.print(g.gyro.z);
//  Serial.println(" rad/s");
//
//  Serial.print("Temperature: ");
//  Serial.print(temp.temperature);
//  Serial.println(" degC");


    //---------------------------------------------------------------------------------

    // EXTRACT VALUES FOR PYTHON SERIAL SEND
    String a1 = String(a.acceleration.y);
    String d1 = String(tfDist3);
    String d2 = String(tfDist);
    Serial.print(d1); 
    Serial.print(","); 

    Serial.print(d2); 
    Serial.print(","); 
    Serial.print(a1);
    Serial.println("");
    Serial.flush();

  
}
