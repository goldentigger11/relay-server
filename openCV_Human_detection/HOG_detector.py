import numpy as np
from cv2 import cv2 as cv2
import imutils
import glob
from pathlib import Path
dir = Path(__file__).parent.absolute()

class Hog_detector():
    def draw_bounding_boxes(self, img:np.array):
        # Initializing the HOG person
        # detector
        hog = cv2.HOGDescriptor()
        hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
        # hog.setSVMDetector(cv2.HOGDescriptor_getDaimlerPeopleDetector())

        # Reading the Image
        image = img 
        
        # Resizing the Image
        image = imutils.resize(image,
                            width=min(400, image.shape[1]))
        
        # Detecting all the regions in the 
        # Image that has a pedestrians inside it
        (regions, _) = hog.detectMultiScale(image, 
                                            winStride=(4, 4),
                                            padding=(4, 4),
                                            scale=1.05)
        
        # Drawing the regions in the Image
        for (x, y, w, h) in regions:
            cv2.rectangle(image, (x, y), 
                        (x + w, y + h), 
                        (0, 0, 255), 2)
        
        return image
    def try_on_images_folder(self, dir_path=f'{dir}/images/'):
        for file in glob.glob(f"{dir_path}*.jpg"):
            img = cv2.imread(file)
            img = self.draw_bounding_boxes(img)
            cv2.imshow("Image", img)
            cv2.waitKey(0)
        cv2.destroyAllWindows()

   

if __name__=='__main__':
    print('in hog file')
    H = Hog_detector()
    H.try_on_images_folder()
