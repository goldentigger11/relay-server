from glob import glob
import keyboard

from os import access
import firebase_admin
from firebase_admin import credentials, db
from firebase_rtdb_pagination import FirebaseRTDBPagination


cred = credentials.Certificate("D:\Downloads\drive-71fa6-firebase-adminsdk-lsczy-62d18f65e0.json")
app = firebase_admin.initialize_app(cred, {'databaseURL': 'https://drive-71fa6-default-rtdb.firebaseio.com/'})



from firebase_admin import db

ref = db.reference("/")
print("connected to DB")
import json
import time
value = 1
acc = -10
steer = 90
bool = True


def take_input():
    global acc
    global steer
    delta_acc = 3
    delta_steer = 2
    if keyboard.is_pressed("w"):
        acc+=delta_acc
    if keyboard.is_pressed("s"):
       acc-=delta_acc
    if keyboard.is_pressed("a"):
        steer -=delta_steer
    if keyboard.is_pressed("d"):
        steer +=delta_steer
    

def make_sure_within_rang(file):
    if file['acc'] >= 50:
        file['acc'] = 50
    if file['acc'] <= 0:
        file['acc'] = 0
    if file['steer'] <= 65:
        file['steer'] = 65
    if file['steer'] >= 115:
        file['steer'] = 115

    
with open("commands.json", "r") as f:
   
    file_contents = json.load(f)
    while True:
        
        # inp = take_input()
        take_input()
        # file_contents["timestamp"] = time.time()
        time_start = time.time()
        # if inp == 'Stop':
        #     break
        # file_contents["acc"] = inp

        file_contents["acc"] = acc
      
        file_contents["steer"] = steer

        file_contents['stamp'] = time.time()
        # steer+=1
        # bool = not bool
        # acc+=1
        make_sure_within_rang(file_contents)

        ref.set(file_contents)
        print(file_contents)

        print(f"{time.time()-time_start}")



