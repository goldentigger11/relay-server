import cv2
import glob
import imutils
import numpy as np
import os
from pathlib import Path
NMS_THRESHOLD=0.3
MIN_CONFIDENCE=0.2


dir = Path(__file__).parent.absolute()
labelsPath = f"{dir}/coco.names"
LABELS = open(labelsPath).read().strip().split("\n")

weights_path = f"{dir}/yolov4-tiny.weights"
config_path = f"{dir}/yolov4-tiny.cfg"

model = cv2.dnn.readNetFromDarknet(config_path, weights_path)
'''
model.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
model.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
'''

layer_name = model.getLayerNames()
l = model.getUnconnectedOutLayers()
layer_name = [layer_name[i[0] - 1] for i in l]
cap = cv2.VideoCapture("streetup.mp4")
writer = None



class Yolo_mini():
    def pedestrian_detection(self, image, model, layer_name, personidz=0):
        (H, W) = image.shape[:2]
        results = []


        blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416),
            swapRB=True, crop=False)
        model.setInput(blob)
        layerOutputs = model.forward(layer_name)

        boxes = []
        centroids = []
        confidences = []

        for output in layerOutputs:
            for detection in output:

                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]

                if classID == personidz and confidence > MIN_CONFIDENCE:

                    box = detection[0:4] * np.array([W, H, W, H])
                    (centerX, centerY, width, height) = box.astype("int")

                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))

                    boxes.append([x, y, int(width), int(height)])
                    centroids.append((centerX, centerY))
                    confidences.append(float(confidence))
        # apply non-maxima suppression to suppress weak, overlapping
        # bounding boxes
        idzs = cv2.dnn.NMSBoxes(boxes, confidences, MIN_CONFIDENCE, NMS_THRESHOLD)
        # ensure at least one detection exists
        if len(idzs) > 0:
            # loop over the indexes we are keeping
            for i in idzs.flatten():
                # extract the bounding box coordinates
                (x, y) = (boxes[i][0], boxes[i][1])
                (w, h) = (boxes[i][2], boxes[i][3])
                # update our results list to consist of the person
                # prediction probability, bounding box coordinates,
                # and the centroid
                res = (confidences[i], (x, y, x + w, y + h), centroids[i])
                results.append(res)
        # return the list of results
        return results

    def draw_bounding_boxes(self, img:np.ndarray):
        image = imutils.resize(img, width=700)
        person_id = LABELS.index("person")
        results = self.pedestrian_detection(image, model, layer_name, person_id)

        for res in results:
            cv2.rectangle(image, (res[1][0],res[1][1]), (res[1][2],res[1][3]), (0, 255, 0), 2)
        return image
    def try_on_images_folder(self, dir_path=f'{dir}/../images/'):
        for file in glob.glob(f"{dir_path}*.jpg"):
            img = cv2.imread(file)
            img = self.draw_bounding_boxes(img)
            cv2.imshow("Image", img)
            cv2.waitKey(0)
        cv2.destroyAllWindows()

if __name__=='__main__':
    Y = Yolo_mini()
    img = cv2.imread(f'{dir}/../images/1.jpg')
    Y.try_on_images_folder()
