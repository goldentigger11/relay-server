import firebase_admin
from firebase_admin import credentials, db
from firebase_rtdb_pagination import FirebaseRTDBPagination

import numpy as np

import struct

cert = '/home/pc-car/Downloads/drive-71fa6-firebase-adminsdk-lsczy-62d18f65e0.json'
#cred = credentials.Certificate("drive-71fa6-firebase-adminsdk-lsczy-5cca64c6f9.json")

cred = credentials.Certificate(cert)
app = firebase_admin.initialize_app(cred, {'databaseURL': 'https://drive-71fa6-default-rtdb.firebaseio.com/'})



ref = db.reference("/")

import json
import time
from serial import Serial
ser_steering = Serial('/dev/ttyUSB0', 9600)
ser_throttle = Serial('/dev/ttyUSB1', 9600)

print(ref.get())
while True:
    try:
        #start = time.time()
        data = ref.get()
        #data['acc'] = 0
        #print(time.time()-start)
        acc = np.uint8(data['acc'])
        #print(data)
        #-------------Sending data---------------
        steer = np.uint8(data['steer'])

        acc += 30

        if acc > 50:
            acc = 50
        
            
        print(acc,steer)
        
        ser_steering.write(struct.pack('>B', int(steer) ))
        
        ser_throttle.write(struct.pack('>B', int(acc)))
        

        ser_steering.flush()
        ser_throttle.flush()
    except Exception as e:
        print("err: ", e)

   
    
