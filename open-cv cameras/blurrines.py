# import the opencv library
import cv2
import time
import numpy as np
from skimage.metrics import structural_similarity as compare_ssim


# define a video capture object
vid = cv2.VideoCapture(0)
vid.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
vid.set(cv2.CAP_PROP_FRAME_HEIGHT, 240) 



vid2 = cv2.VideoCapture(2)
vid2.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
vid2.set(cv2.CAP_PROP_FRAME_HEIGHT, 240)


vid3 = cv2.VideoCapture(4)
vid3.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
vid3.set(cv2.CAP_PROP_FRAME_HEIGHT, 240)

old_frame= None
while(True):
	
    # Capture the video frame
    # by frame
    ret, frame1 = vid.read()
    
    ret, frame2 = vid2.read()
    ret, frame3 = vid3.read()
    #frame = cv2.blur(frame, (5, 5))
    
    # batee5 = cv2.Laplacian(frame1, cv2.CV_64F).var()
    # print(batee5)



    # DIsplay -------------------------------
    frame = np.concatenate((frame2,frame1, frame3), axis=0)
    frame = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    # Display the resulting frame
    cv2.imshow('frame', frame)
    # time.sleep(0.1)

    # --------- additions
    
    frame1 = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
    if old_frame is None:
        old_frame = frame1
    (score, diff) = compare_ssim(frame1, old_frame, full=True)
    diff = (diff * 255).astype("uint8")
    print("SSIM: {}".format(score))


 
    # the 'q' button is set as the
    # quitting button you may use any
    # desired button of your choice
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    old_frame = frame1
# After the loop release the cap object
vid.release()
# Destroy all the windows
cv2.destroyAllWindows()
